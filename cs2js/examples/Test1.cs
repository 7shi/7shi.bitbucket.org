﻿enum Token { None, Symbol }

class Test1
{
    public void foo() { alert(Token.Symbol); }
}

class Test2
{
    public int a;

    public Test2(int a)
    {
        this.a = a;
    }

    public void foo()
    {
        alert(this.a);
    }
}
